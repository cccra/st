# Gus's build of st - the simple (suckless) terminal

# Complete list of Patches
- Scrollback
- Alpha focus
- Font2
- Xresources
- Vertcenter
- Bold-is-not-bright
- Anysize
- Anysize-nobar
- Boxdraw
- Desktopentry
- Externalpipe
- Iso14755
- Swapmouse
